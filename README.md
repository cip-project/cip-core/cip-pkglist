# cip-pkglist

This repository includes Debian package list which are the target of
CIP's long-term maintenance and helper scripts for proposing or
registering such packages.

## Preparation

Install packages required by helper scripts.

```shell
# User and maintainer
sudo apt install python3-apt python3-yaml debootstrap python3-docker

# Maintainer only
sudo apt install debootstrap distro-info
```

Additionally, Docker engine must also be installed for running the cleanroom container

## Initialization (Maintainer)

Maintainers need to initialize resources for each Debian `SUITE` (e.g. jessie).

First, generate an ELTS base tree.

```shell
sudo ./create-elts-base.sh ${SUITE}
sudo tar czf elts-base_${SUITE}.tar.gz -C elts-base_${SUITE} .
sudo rm -rf elts-base_${SUITE}
```

Second, convert the tree to a Docker image then register it as a "cleanroom".
The registered image will be used by users, and updated by maintainers.

```shell
docker login registry.gitlab.com # once
docker import - registry.gitlab.com/cip-project/cip-core/cip-pkglist/cleanroom-${SUITE}:latest < elts-base_${SUITE}.tar.gz
docker push registry.gitlab.com/cip-project/cip-core/cip-pkglist/cleanroom-${SUITE}:latest
sudo rm elts-base_${SUITE}.tar.gz
```

At the moment, no package has been proposed yet.
If CIP has decided to support the target `SUITE` already,
create the first proposal that only consists of the "ELTS base packages".

```shell
./generate-proposal.py init ${SUITE} "Your Name"
```

## Package Proposal (Users)

If there is a pkginfo_<SUITE>.yml already available for the SUITE, then use the below command to create a clean room
image with the packages in pkginfo_<SUITE>.yml file installed.

    $ ./pkginfo.py update <SUITE>

Prepare `proposal-base.yml` file with only the top level source and binary packages.

Example of the input proposal-base.yml file:
```
date: 2025/03/07
proposer: Test
pdp_revision: '5.0'
debian_version: bookworm
src_pkgs:
  srcPkgA:
    bin_pkgs:
      - binPkgA
      - binPkgB
      - binPkgC
    in_target: 'True'
    reason: "The reasons to propose this package"
  srcPkgB:
    bin_pkgs:
      - binPkgA
    in_target: 'True'
    reason: "The reasons to propose this package"
```

Generate your proposal information.

    $ ./generate-proposal.py <proposal-base.yml>

Create the `proposal-base.yml` as described above and pass the file to `generate-proposal.py`, then you get the proposal file `proposal.yml`.
Please send this file to CIP Core WG to proceed the review process.

If the proposal is for a new SUITE which does not already have a pkginfo_<SUITE>.yml file, Use the below command to generate the proposal

    $ ./generate-proposal.py init <SUITE> <PROPOSER_NAME>

Package Registration
====================

After the proposal accepted, we can register the package information
to the CIP maintained package list using `pkginfo.py`.

    $ ./pkginfo.py add-proposal proposal.yml

To update other PDP information, update the docker clean room image and
create the pkglist in ELTS format:

    $ ./pkginfo.py update <SUITE>

You can check the package information registered in the list
using the same script. In case of Debian 10 buster:

    $ ./pkginfo.py show buster libssl1.1 busybox
