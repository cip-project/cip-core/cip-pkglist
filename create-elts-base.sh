#!/bin/sh

set -e

SUITE=${1}
TREE=elts-base_${SUITE}

# Included in the ELTS's base packages
BASE_PKGS_APPEND="sudo openssh-server ca-certificates"

KEYRING_URL=https://deb.freexian.com/extended-lts/pool/main/f/freexian-archive-keyring/freexian-archive-keyring_2022.06.08_all.deb
KEYRING_PKG=$(basename ${KEYRING_URL})
KEYRING_PKG_SHA256=a8160d1aa1a40aa9988bf0b389b650550c7460ec3b4ec1d847778fe44b9c4dbc

if [ -z "${SUITE}" ]; then
	echo "Usage: ${0} <suite>"
	exit 1
fi

if [ "$(whoami)" != "root" ]; then
	echo "Please run as root"
	exit 1
fi

# Support not only suites in ELTS phase but also the newers so that
# users can create package proposals even those suites are not ELTS yet.
mirror_debootstrap=http://deb.debian.org/debian/
mirror_elts=
if ! elts_suites=$(debian-distro-info --elts); then
	echo "Failed to get ELTS suites with debian-distro-info"
	exit 1
fi
for elts_suite in ${elts_suites}; do
	if [ "${elts_suite}" = "${SUITE}" ]; then
		mirror_debootstrap=http://archive.debian.org/debian/
		mirror_elts=http://deb.freexian.com/extended-lts
		break
	fi
done

# Use debian-archive-removed-keys.gpg only when the key is expired
if [ -f /usr/share/keyrings/debian-archive-${SUITE}-stable.gpg ]; then
	archive_keyring=/usr/share/keyrings/debian-archive-${SUITE}-stable.gpg
else
	archive_keyring=/usr/share/keyrings/debian-archive-removed-keys.gpg
	apt_line_opts="[trusted=yes]"
fi

# NOTE: Don't specify any "--variant"
debootstrap --keyring=${archive_keyring} ${SUITE} ${TREE} ${mirror_debootstrap}

sed -i "s@^deb @deb ${apt_line_opts} @" ${TREE}/etc/apt/sources.list

if [ -n "${mirror_elts}" ]; then
	# Install freexian archive GPG key
	# See https://www.freexian.com/lts/extended/docs/how-to-use-extended-lts/
	wget --no-check-certificate ${KEYRING_URL}
	if ! sha256sum ${KEYRING_PKG} | grep -q "^${KEYRING_PKG_SHA256} "; then
		echo "ERROR: Checksum of downloaded package does not match"
		exit 1
	fi
	mv ${KEYRING_PKG} ${TREE}
	chroot ${TREE} dpkg -i /${KEYRING_PKG}
	rm ${TREE}/${KEYRING_PKG}
	echo "deb ${mirror_elts} ${SUITE}-lts main" >> ${TREE}/etc/apt/sources.list
fi

chroot ${TREE} sh -c " \
	apt-get update && \
	apt-get upgrade -y && \
	apt-get install -y ${BASE_PKGS_APPEND} && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*
"

echo "Done"
