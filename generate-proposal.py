#!/usr/bin/env python3
#
# generate-proposal.py
#
# A helper script to generate a proposal file with the specified format,
# which is required for the package proposal in the CIP PDP.
#
# Usage:
#   $ ./generate-proposal.py <proposal-base.yml>
#
# Finally, this script generates proposal.yml which includes
# the required information for the package proposal.
#
# Copyright (c) 2019 TOSHIBA Corporation
#
# SPDX-License-Identifier: Apache-2.0
#

import common
import datetime
import count_cve
import sys
import yaml
from datetime import datetime

# Local modules
import cleanroom

PROPOSAL_FILE = "proposal.yml"
PROPOSAL_SUITE = ""
PROPOSER = ""

def die(text):
    print(common.ERROR_TAG + text)
    exit(1)


def is_dep_parent_pkg_in_rootfs(dep_pkg_dict, pr_request_info):
    """
    Verify if the given dependency source package parent package is selected as in_rootfs
    :param dep_pkg_dict:
    :param pr_request_info:
    :return:
    """
    for dep_bin_pkg, dep_par_pkg_info_list in dep_pkg_dict.items():
        for dep_par_pkg_info in dep_par_pkg_info_list:
            if dep_par_pkg_info[1] in pr_request_info.proposed_src_pkgs:
                if pr_request_info.proposed_src_pkgs[dep_par_pkg_info[1]].in_target:
                    return True
    return False


def prepare_src_pkg_info(apt, cve, deb_src_pkg_name, bin_pkg_list, in_target='', reason='', prop_src_pkg_info={}, src_bin_pkg_obj_deps={}):

    if prop_src_pkg_info:
        src_pkg_info = prop_src_pkg_info
        deb_bin_pkg_dict = prop_src_pkg_info.bin_pkg_dict
        reason = prop_src_pkg_info.reason + '\n' + reason
    else:
        src_pkg_info = common.PDPProposal.SrcPkgInfo()
        deb_bin_pkg_list = list()

    temp = src_bin_pkg_obj_deps[deb_src_pkg_name]
    src_pkg_info.bin_pkg_list = []
    src_pkg_info.bin_pkg_list = src_bin_pkg_obj_deps[deb_src_pkg_name]["binpkgs"]

    if in_target == "False":
        security_criteria = "-"
        n_cve = "-"
    else:
        security_criteria = common.evaluate_security_criteria(apt, list(src_pkg_info.bin_pkg_list))
        # Get the number of CVE's available for the source package
        n_cve = str(cve.count_cve(deb_src_pkg_name))

    src_pkg_info.in_target = in_target
    src_pkg_info.security_criteria = security_criteria
    src_pkg_info.n_cve = n_cve
    src_pkg_info.reason = reason
    return src_pkg_info

def create_proposal_deps(pr_request_info, clean_room, pdp_load_success):
    proposed_bin_pkgs_list = []
    proposed_src_bin_pkg_obj = {}
    for pkgs in pr_request_info.proposed_src_pkgs:
        proposed_src_bin_pkg_obj[pkgs] = {}
        for bin_pkg in pr_request_info.proposed_src_pkgs[pkgs].bin_pkg_list:
            proposed_bin_pkgs_list.append(bin_pkg)
            proposed_src_bin_pkg_obj[pkgs]["binpkgs"] = []
            proposed_src_bin_pkg_obj[pkgs]["binpkgs"].append(bin_pkg)

    dpkg_query_before_installation = clean_room.run_dpkg_query_command()
    dpkg_query_before_installation = dpkg_query_before_installation.split("\n")

    print("Proposed packages are being installed in the clean room container...")
    clean_room.install_packages_inside_clean_room(proposed_bin_pkgs_list, pr_request_info.proposed_debian_version)

    dpkg_query_after_installation = clean_room.run_dpkg_query_command()
    dpkg_query_after_installation = dpkg_query_after_installation.split("\n")

    diff_dpkg_query = common.dpkg_query_diff(dpkg_query_before_installation, dpkg_query_after_installation)
    (src_bin_pkg_obj_dep, bin_pkg_src_pkg_mapping) = common.get_source_pkg_from_bin_pkg(diff_dpkg_query)
    merged_src_bin_pkg_obj = merge_dict(src_bin_pkg_obj_dep, proposed_src_bin_pkg_obj)
    return merged_src_bin_pkg_obj

def merge_dict(dict1, dict2):
    merged_src_bin_pkg_obj = dict2.copy()
    for key, value in dict1.items():
        if key in merged_src_bin_pkg_obj:
            # Convert to set to remove duplicates, then back to list
            merged_src_bin_pkg_obj[key]["binpkgs"] = list(set(merged_src_bin_pkg_obj[key]["binpkgs"] + value["binpkgs"]))
        else:
            merged_src_bin_pkg_obj[key] = {}
            merged_src_bin_pkg_obj[key]["binpkgs"] = value["binpkgs"]

    return merged_src_bin_pkg_obj

def remove_duplicate_packages(pdp_info, pdp_load_success, src_bin_pkg_obj_deps):
    pdp_info_bin_pkg_list = []
    if pdp_load_success:
        pdp_info_bin_pkg_list = [bp for bp_info in pdp_info.get_pdp_info().values() for bp in bp_info.bin_pkg_data_dict]
    binary_packages = set()
    for source_package, binaries in src_bin_pkg_obj_deps.items():
        binary_packages.update(binaries["binpkgs"])
    prop_info_bin_pkg_list_rec_deps = list(binary_packages)

    pdp_info_bin_pkg_set = set(pdp_info_bin_pkg_list)
    unique_elements = [item for item in prop_info_bin_pkg_list_rec_deps if item not in pdp_info_bin_pkg_set]

    for src_pkg in list(src_bin_pkg_obj_deps.keys()):
        bin_pkgs = list(src_bin_pkg_obj_deps[src_pkg]["binpkgs"])
        for pkg in bin_pkgs:
            if pkg not in unique_elements:
                src_bin_pkg_obj_deps[src_pkg]["binpkgs"].remove(pkg)

        bin_pkgs = list(src_bin_pkg_obj_deps[src_pkg]["binpkgs"])
        if len(bin_pkgs) == 0:
            del src_bin_pkg_obj_deps[src_pkg]
    return src_bin_pkg_obj_deps

def generate_proposal(pr_request_info, initial_proposal):
    """
    Generate the proposal file by taking proposal-base.yaml from the user
    :param pr_request_info: object of common.PDPProposal.ProposalInfo
    :return:
    """
    apt = common.Apt()
    cve = count_cve.Cve()
    cve.load_cve_data()

    try:
        if not apt.apt_initialize(PROPOSAL_SUITE):
            del apt
            die("Apt initialize is failed")

        if pr_request_info:
            # Print proposer name
            print("Proposer name: " + pr_request_info.proposer_name)

            # Print proposal date
            print("Proposal date: " + pr_request_info.proposal_date)

            # Print debian version
            print("Proposed Debian version: " + pr_request_info.proposed_debian_version)

            pdp_info = common.PDPInfo(pr_request_info.proposed_debian_version)
            pdp_load_success = pdp_info.load_pdp()
            proposed_bin_pkgs_list = []
            # Loop over the proposed source packages and binary packages and check if they are valid packages
            for src_pkg in pr_request_info.proposed_src_pkgs.keys():
                src_pkg_name, src_pkg_ver, src_pkg_bin_list = apt.apt_cache_get_src_info(src_pkg)
                # Check if the source package is valid
                if src_pkg_name == "" or src_pkg_name != src_pkg:
                    die("%s is not valid source package!!!" % (src_pkg))

                # Check if the binary packages entered are valid
                for bin_pkg in pr_request_info.proposed_src_pkgs[src_pkg].bin_pkg_list:
                    if bin_pkg not in src_pkg_bin_list:
                        die("%s is not valid binary package from source package %s!!!" % (bin_pkg, src_pkg))
                    else:
                        proposed_bin_pkgs_list.append(bin_pkg)

            # create proposal_deps
            src_bin_pkg_obj_deps = create_proposal_deps(pr_request_info, clean_room, pdp_load_success)

            # create proposal_deps.yml file
            proposal_deps_file = "proposal_deps.yml"
            with open(proposal_deps_file, 'w') as prop_deps:
                yaml.dump(src_bin_pkg_obj_deps, prop_deps, default_flow_style=False )
                print("Dependencies of proposed packages are saved to " + proposal_deps_file)

            # remove binary packages that are already in the pkginfo_<SUITE>.yml from the proposal
            src_bin_pkg_obj_deps = remove_duplicate_packages(pdp_info, pdp_load_success, src_bin_pkg_obj_deps)

            # update the reason of dependency package
            dependency_package_reason = "Dependencies of "+', '.join(proposed_bin_pkgs_list)

            for src_pkg in list(pr_request_info.proposed_src_pkgs.keys()):
                if src_pkg not in src_bin_pkg_obj_deps:
                    # then they are duplicate source package in proposal, remove them
                    del pr_request_info.proposed_src_pkgs[src_pkg]

            for src_pkg in src_bin_pkg_obj_deps:
                if src_pkg in pr_request_info.proposed_src_pkgs.keys():
                    reason = pr_request_info.proposed_src_pkgs[src_pkg].reason
                    in_target = pr_request_info.proposed_src_pkgs[src_pkg].in_target
                else:
                    reason = dependency_package_reason
                    if "reason" in src_bin_pkg_obj_deps[src_pkg]:
                        reason = src_bin_pkg_obj_deps[src_pkg]["reason"]
                    in_target = 'True'

                pr_request_info.proposed_src_pkgs[src_pkg] = prepare_src_pkg_info(apt, cve, src_pkg, {}, in_target=in_target, reason=reason, prop_src_pkg_info={}, src_bin_pkg_obj_deps=src_bin_pkg_obj_deps)

        # init mode
        else:
            # List of srcpkg,binpkg,tmpver. TODO: Remove unused tmpver from all codes.
            installed_pkgs = [pkg + ',tmpver' for pkg in cleanroom.get_installed_pkgs(PROPOSAL_SUITE)]

            initial_clean_room_packages = []
            (initial_clean_room_packages, bin_pkg_src_pkg_mapping) = common.get_source_pkg_from_bin_pkg(installed_pkgs)
            proposed_src_bin_pkg_obj = initial_clean_room_packages

            proposal_info_dict = dict()
            proposal_info_dict["proposer"] = PROPOSER
            proposal_info_dict["date"] = datetime.now().strftime("%Y/%m/%d")
            proposal_info_dict["debian_version"] = PROPOSAL_SUITE
            proposal_info_dict["src_pkgs"] = dict()
            for srcpkg in proposed_src_bin_pkg_obj:
                proposal_info_dict["src_pkgs"][srcpkg] = dict()
                proposal_info_dict["src_pkgs"][srcpkg]["bin_pkgs"] = proposed_src_bin_pkg_obj[srcpkg]["binpkgs"]
                proposal_info_dict["src_pkgs"][srcpkg]["reason"] = 'This package is a part of "ELTS base package" that is a minimum set of CIP support'
                proposal_info_dict["src_pkgs"][srcpkg]["in_target"] = "True"

                proposed_src_bin_pkg_obj[srcpkg]["reason"] = proposal_info_dict["src_pkgs"][srcpkg]["reason"]
                proposed_src_bin_pkg_obj[srcpkg]["in_target"] = proposal_info_dict["src_pkgs"][srcpkg]["in_target"]

            pr_request_info = common.PDPProposal().dict_to_proposal_info(proposal_info_dict, True) 
            for srcpkg in proposed_src_bin_pkg_obj:
                in_target = proposed_src_bin_pkg_obj[srcpkg]["in_target"]
                reason = proposed_src_bin_pkg_obj[srcpkg]["reason"]
                pr_request_info.proposed_src_pkgs[srcpkg] = prepare_src_pkg_info(apt, cve, srcpkg, {}, in_target=in_target, reason=reason, prop_src_pkg_info={}, src_bin_pkg_obj_deps=proposed_src_bin_pkg_obj)
    finally:
        del apt

    print("\n")
    print("################################################")
    print("# Final proposal request")
    print("################################################")
    common.PDPProposal().print_req_info(pr_request_info)
    print("################################################")
    common.PDPProposal().save(pr_request_info, PROPOSAL_FILE)
    print("Proposal Saved in file: " + PROPOSAL_FILE)

def usage():
    print("Usage: ")
    print("To generate proposal with a proposal-base.yml file")
    print("     ./generate-proposal <proposal-base.yml>")
    print("To generate initial proposal for a SUITE")
    print("     ./generate-proposal init <SUITE> <PROPOSER_NAME>")
    exit(1)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        # proposal with .yml file
        if sys.argv[1].endswith(".yml"):
            prop_info = common.PDPProposal().load(sys.argv[1], proposal_base=True)
            PROPOSAL_SUITE = prop_info.proposed_debian_version
            generate_proposal(pr_request_info=prop_info, initial_proposal=False)
        else:
            usage()
    else:
        # initial proposal
        if sys.argv[1] == "init" and sys.argv[2] != "" and sys.argv[3] != "":
            if sys.argv[2] in common.DEBIAN_CODE_NAMES:
                PROPOSAL_SUITE = sys.argv[2]
                PROPOSER = (" ").join(sys.argv[3:])
                generate_proposal(pr_request_info=None, initial_proposal=True)
            else:
                usage()
        else:
            usage()
