#!/usr/bin/env python3

import docker

DOCKER_REGISTRY="registry.gitlab.com/cip-project/cip-core/cip-pkglist"

def docker_run(suite, commands):
    outputs = ""
    try:
        client = docker.from_env()
        # Always removed when stopped
        container = client.containers.run(
            image=f'{DOCKER_REGISTRY}/cleanroom-{suite}',
            command=f'tail -f /dev/null',
            remove=True, detach=True, name=f'cip-pkglist-cleanroom-{suite}')
        for cmd in commands:
            exit_code, output = container.exec_run(cmd=cmd)
            if (exit_code != 0):
                print(f'ERROR: docker exec failed ({exit_code}): {output}')
                outputs = None
                break
            outputs += output.decode('utf-8').rstrip('\n')
    except Exception as e:
        print(f'ERROR: docker run failed: {e}')
        outputs = None
    container.stop()
    return outputs

# Returns a list of installed "source,binary" package pairs
def get_installed_pkgs(suite):
    commands = ["dpkg-query -W -f='${source:Package},${Package}\n'"]
    outputs = docker_run(suite, commands)
    if outputs is None:
        return None
    return outputs.split('\n')

# Returns a list of "source,binary" package pairs additionally installed
# with binpkgs. binpkgs are not included in the returned list.
def get_dependencies(suite, binpkgs):
    # To be implemented
    return None
